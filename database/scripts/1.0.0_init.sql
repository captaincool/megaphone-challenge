create table sale(
  id serial primary key,
  quantity int not null,
  price numeric(11, 2) not null CHECK (price >= 0),
  product_description text not null,
  customer_name text not null,
  merchant_name text not null,
  merchant_address text not null,
  created_date timestamp not null,
  modified_date timestamp,
  deleted_date timestamp
);