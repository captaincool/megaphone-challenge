## System Requirements

In order to run this application, please ensure you have the latest version of [Docker installed](https://docs.docker.com/docker-for-mac/install/).

## Running the application

While in the root directory, run `make start`. Once completed, respective services should be ready for incoming connections at the exposed ports. For the sake of this challenge, the ports and configurations are not configurable, but can be quickly improved later.

- Database: http://localhost:5432
- API: http://localhost:8080
- Frontend: http://localhost:3000

You can test the result by opening http://localhost:3000, then uploading `example-data.csv` (located at the root of this project) via the sidebar.

## Stopping the application

While in the root directory, run `make stop`.

## Destroying the application

To uninstall, please run `make environment-clean`

## Development

For instructions and details on how to actively develop on this application, please look at the "readme"s inside the `frontend` and `api` directories.
