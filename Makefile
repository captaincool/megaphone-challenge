environment-clean:
	docker-compose down --rmi all

start:
	docker-compose up

stop:
	docker-compose stop

build:
	docker-compose build