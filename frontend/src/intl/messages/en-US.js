export const intlMessagesEnUS = {
  sales: "Sales",
  revenue: "Total Revenue: {value}",
  dateCreated: "Date Added",
  customerName: "Customer Name",
  product: "Product",
  merchantName: "Merchant Name",
  merchantAddress: "Merchant Address",
  quantity: "Quantity",
  price: "Price",
  noResults: "No Results",
  fetchError: "Could not load results",
  importData: "Import Data",
  importDataDescription:
    'Import data by choosing a CSV and clicking "Submit" below',
  chooseFile: "Choose File",
  selectedFile: "Selected: {fileName}",
  submit: "Submit",
  uploadRowsSuccess:
    "Successfully imported {value} {value, plural, one {row} other {rows}}",
  uploadRowsEmpty: "No new data was added",
  uploadRowsError: "There was a problem, please try again later"
};
