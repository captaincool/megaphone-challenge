import { intlMessagesEnUS } from "./messages/en-US";

export const intlMessages = Object.keys(intlMessagesEnUS).reduce((acc, cur) => {
  acc[cur] = cur;
  return acc;
}, {});
