import React, { Component } from "react";
import { FormattedMessage, FormattedDate, FormattedNumber } from "react-intl";
import Grid from "@material-ui/core/Grid";
import { SalesTableContainer } from "./table/container";
import { SalesImportContainer } from "./import/container";
import { salesInjectApi } from "./inject-api";
import { intlMessages } from "../intl/messages";
import Typography from "@material-ui/core/Typography";

class SalesPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      readyState: "pending",
      rows: []
    };
  }

  componentDidMount() {
    this.fetchData();
  }

  render() {
    return (
      <React.Fragment>
        <Typography variant="h3" gutterBottom>
          <FormattedMessage id={intlMessages.sales} />
        </Typography>

        <Grid container spacing={2}>
          <Grid item xs={8}>
            <SalesTableContainer
              rows={this.state.rows}
              columns={[
                {
                  title: <FormattedMessage id={intlMessages.dateCreated} />,
                  key: "created_date",
                  valueRenderer: date => <FormattedDate value={date} />
                },
                {
                  title: <FormattedMessage id={intlMessages.customerName} />,
                  key: "customer_name"
                },
                {
                  title: <FormattedMessage id={intlMessages.product} />,
                  key: "product_description"
                },
                {
                  title: <FormattedMessage id={intlMessages.merchantName} />,
                  key: "merchant_name"
                },
                {
                  title: <FormattedMessage id={intlMessages.merchantAddress} />,
                  key: "merchant_address"
                },
                {
                  title: <FormattedMessage id={intlMessages.quantity} />,
                  key: "quantity",
                  align: "right",
                  valueRenderer: value => (
                    <FormattedNumber
                      value={value}
                      style="currency"
                      currency="USD"
                    />
                  )
                },
                {
                  title: <FormattedMessage id={intlMessages.price} />,
                  key: "price",
                  align: "right",
                  valueRenderer: value => (
                    <FormattedNumber
                      value={value}
                      style="currency"
                      currency="USD"
                    />
                  )
                }
              ]}
            />
          </Grid>
          <Grid item xs={4}>
            <SalesImportContainer onUpdateData={() => this.fetchData()} />
          </Grid>
        </Grid>
      </React.Fragment>
    );
  }

  async fetchData() {
    this.setState({
      readyState: "loading"
    });

    const { data } = await this.props.salesApi.findAll();

    this.setState({
      rows: data,
      readyState: "pending"
    });
  }
}

export const SalesPageContainer = salesInjectApi(SalesPage);
