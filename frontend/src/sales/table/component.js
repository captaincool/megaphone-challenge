import React, { Component } from "react";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Paper from "@material-ui/core/Paper";
import CircularProgress from "@material-ui/core/CircularProgress";
import { FormattedNumber, FormattedMessage } from "react-intl";
import { intlMessages } from "../../intl/messages";
import { arrayIsEqual } from "../../helper/array/is-equal";
import { readyStateEnum } from "../../helper/ready-state.enum";

export class SalesTableComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      readyState: readyStateEnum.pending,
      revenue: null
    };
  }

  componentDidMount() {
    this.fetchRevenueData();
  }

  componentDidUpdate(prevProps) {
    if (!arrayIsEqual(this.props.rows, prevProps.rows, row => row.id)) {
      this.fetchRevenueData();
    }
  }

  render() {
    const { revenue, readyState, error } = this.state;
    const { rows = [], columns = [] } = this.props;

    return (
      <TableContainer component={Paper}>
        <Table size="small">
          <TableHead>
            <TableRow>
              {columns.map(({ title, align, key }) => (
                <TableCell key={key} align={align}>
                  {title}
                </TableCell>
              ))}
            </TableRow>
          </TableHead>
          <TableBody>
            {readyState === readyStateEnum.pending &&
              rows.map(row => (
                <TableRow key={row.id}>
                  {columns.map(({ key, align, valueRenderer = v => v }) => (
                    <TableCell key={key} align={align}>
                      {valueRenderer(row[key])}
                    </TableCell>
                  ))}
                </TableRow>
              ))}

            {rows.length === 0 && (
              <TableRow>
                <TableCell align="center" colSpan={columns.length}>
                  {readyState === readyStateEnum.loading && (
                    <CircularProgress />
                  )}
                  {readyState === readyStateEnum.error && (
                    <FormattedMessage id={intlMessages.fetchError} />
                  )}
                  {readyState === readyStateEnum.pending && (
                    <FormattedMessage id={intlMessages.noResults} />
                  )}
                </TableCell>
              </TableRow>
            )}
            <TableRow>
              <TableCell colSpan={columns.length} align="right">
                <FormattedMessage
                  id={intlMessages.revenue}
                  values={{
                    value:
                      revenue !== null ? (
                        <FormattedNumber
                          value={revenue}
                          style="currency"
                          currency="USD"
                        />
                      ) : (
                        "--"
                      )
                  }}
                />
              </TableCell>
            </TableRow>
          </TableBody>
        </Table>
      </TableContainer>
    );
  }

  async fetchRevenueData() {
    this.setState({
      readyState: readyStateEnum.loading,
      error: false
    });

    try {
      const { data } = await this.props.salesApi.calculateRevenue();

      this.setState({
        revenue: data.revenue,
        readyState: readyStateEnum.pending
      });
    } catch (e) {
      this.setState({
        readyState: readyStateEnum.error
      });
    }
  }
}
