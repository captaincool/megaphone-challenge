import { SalesTableComponent } from "./component";
import { salesInjectApi } from "../inject-api";

export const SalesTableContainer = salesInjectApi(SalesTableComponent);
