import { jsonFetch } from "../helper/json/fetch";

export const SalesApi = ({ baseUrl }) => ({
  /**
   * Fetch all the available sale records
   */
  findAll() {
    return jsonFetch(`${baseUrl}/api/v1/sale`, {
      headers: {
        "Content-Type": "application/json"
      }
    });
  },
  /**
   * Calculate the total sales revenue
   */
  calculateRevenue() {
    return jsonFetch(`${baseUrl}/api/v1/sale/aggregate/revenue`, {
      headers: {
        "Content-Type": "application/json"
      }
    });
  },
  /**
   * Upload a CSV file to batch import a list of records
   */
  uploadCSV(fileData) {
    const data = new FormData();

    data.append("file", fileData);

    return jsonFetch(`${baseUrl}/api/v1/import/sale`, {
      method: "POST",
      body: data
    });
  }
});
