import React, { Component } from "react";
import Button from "@material-ui/core/Button";
import Grid from "@material-ui/core/Grid";
import Snackbar from "@material-ui/core/Snackbar";
import { intlMessages } from "../../intl/messages";
import { FormattedMessage } from "react-intl";
import Typography from "@material-ui/core/Typography";
import { readyStateEnum } from "../../helper/ready-state.enum";

export class SalesImportComponent extends Component {
  constructor(props) {
    super(props);
    this.fileInput = React.createRef();
    this.state = {
      readyState: readyStateEnum.pending,
      isSnackbarOpen: false,
      selectedFile: null,
      rowsAdded: []
    };
  }

  render() {
    const { isSnackbarOpen, selectedFile } = this.state;
    return (
      <div>
        <Typography variant="h4" gutterBottom>
          <FormattedMessage id={intlMessages.importData} />
        </Typography>

        <Typography variant="subtitle1">
          <FormattedMessage id={intlMessages.importDataDescription} />
        </Typography>

        <form onSubmit={e => this.onSubmitForm(e)}>
          {selectedFile && (
            <Grid item>
              <Typography variant="body1">
                <FormattedMessage
                  id={intlMessages.selectedFile}
                  values={{
                    fileName: selectedFile.name
                  }}
                />
              </Typography>
            </Grid>
          )}
          <Grid item>
            <Button variant="contained" component="label" disableElevation>
              <FormattedMessage id={intlMessages.chooseFile} />
              <input
                ref={this.fileInput}
                accept=".csv"
                type="file"
                style={{ display: "none" }}
                onChange={e => {
                  let selectedFile = null;
                  if (this.fileInput.current.files.length) {
                    selectedFile = this.fileInput.current.files[0];
                  }
                  this.setState({
                    selectedFile
                  });
                }}
              />
            </Button>
          </Grid>
          <Grid item>
            <Button
              variant="contained"
              color="secondary"
              type="submit"
              disableElevation
            >
              <FormattedMessage id={intlMessages.submit} />
            </Button>
          </Grid>
        </form>

        <Snackbar
          anchorOrigin={{
            vertical: "bottom",
            horizontal: "center"
          }}
          open={isSnackbarOpen}
          autoHideDuration={3000}
          onClose={this.onCloseSnackbar}
          message={this.renderSnackbarMessage()}
        />
      </div>
    );
  }

  renderSnackbarMessage() {
    const { readyState, rowsAdded } = this.state;

    if (readyState === readyStateEnum.error) {
      return <FormattedMessage id={intlMessages.uploadRowsError} />;
    }

    if (!rowsAdded.length) {
      return <FormattedMessage id={intlMessages.uploadRowsEmpty} />;
    }

    return (
      <FormattedMessage
        id={intlMessages.uploadRowsSuccess}
        values={{
          value: rowsAdded.length
        }}
      />
    );
  }

  async importData() {
    this.setState({
      readyState: readyStateEnum.loading,
      rowsAdded: []
    });

    try {
      const { created } = await this.props.salesApi.uploadCSV(
        this.state.selectedFile
      );

      this.fileInput.current.value = null;

      this.setState({
        selectedFile: null,
        isSnackbarOpen: true,
        readyState: readyStateEnum.pending,
        rowsAdded: created
      });

      this.props.onUpdateData(created);
    } catch (e) {
      this.setState({
        selectedFile: null,
        isSnackbarOpen: true,
        readyState: readyStateEnum.error
      });
    }
  }

  onSubmitForm = e => {
    e.preventDefault();

    this.importData();
  };

  onCloseSnackbar = () => {
    this.setState({
      isSnackbarOpen: false
    });
  };
}
