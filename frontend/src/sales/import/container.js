import { SalesImportComponent } from "./component";
import { salesInjectApi } from "../inject-api";

export const SalesImportContainer = salesInjectApi(SalesImportComponent);
