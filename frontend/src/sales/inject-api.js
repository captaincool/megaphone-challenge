import { SalesApi } from "./api";
import React from "react";
import { configuration } from "../configuration";

/**
 * Inject the sales api as a prop into @param WrappedComponent
 */
export const salesInjectApi = WrappedComponent => {
  const api = SalesApi({
    baseUrl: configuration().api.base
  });

  return props => <WrappedComponent {...props} salesApi={api} />;
};
