export const configuration = () => ({
  api: {
    base: process.env.REACT_APP_API_BASE_URL
  }
});
