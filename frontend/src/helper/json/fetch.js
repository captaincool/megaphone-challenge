/**
 * A simple Fetch API wrapper to parse and return JSON responses
 */
export const jsonFetch = (...args) =>
  fetch(...args).then(async response => response.json());
