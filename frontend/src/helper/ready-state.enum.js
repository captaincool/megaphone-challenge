export const readyStateEnum = {
  pending: "pending",
  loading: "loading",
  error: "error"
};
