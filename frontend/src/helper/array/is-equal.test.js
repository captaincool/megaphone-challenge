import { arrayIsEqual } from "./is-equal";

describe("arrayIsEqual", () => {
  it("should determine if two arrays are equal by comparing their values", () => {
    expect(arrayIsEqual([1, 2, 3], [1, 2, 3])).toBe(true);
    expect(arrayIsEqual([1, 2, 3], [1, 2])).toBe(false);
  });

  it("should return false if the arrays are not in the same order", () => {
    expect(arrayIsEqual([1, 2, 3], [3, 2, 1])).toBe(false);
  });

  it("should accept a third argument which maps how the comparator is evaluated", () => {
    const mockArr1 = [
      {
        key: "test",
        value: 1
      }
    ];

    const mockArr2 = [
      {
        key: "test",
        value: 1
      }
    ];

    expect(arrayIsEqual(mockArr1, mockArr2, item => item.key)).toBe(true);
  });
});
