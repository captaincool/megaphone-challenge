/**
 * Determine if two arrays are equal by comparing each of their items against a mapped value
 */
export const arrayIsEqual = (arr1, arr2, predicate = item => item) => {
  if (arr1.length !== arr2.length) {
    return false;
  }

  return arr1.every((item, i) => predicate(item) === predicate(arr2[i]));
};
