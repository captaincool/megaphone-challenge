import React from "react";
import ReactDOM from "react-dom";
import { IntlProvider } from "react-intl";
import { intlMessagesEnUS } from "./intl/messages/en-US";
import Container from "@material-ui/core/Container";
import { SalesPageContainer } from "./sales/page";

ReactDOM.render(
  <IntlProvider locale="en" messages={intlMessagesEnUS}>
    <Container>
      <SalesPageContainer />
    </Container>
  </IntlProvider>,
  document.getElementById("root")
);
