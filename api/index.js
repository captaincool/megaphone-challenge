require("dotenv").config();
const express = require("express");
const cors = require("cors");
const bodyParser = require("body-parser");
const app = express();
const configuration = require("./app/configuration");

const saleRouter = require("./app/sale/router");
const importRouter = require("./app/import/router");

const router = express.Router();

router.use("/api/v1/sale", saleRouter());
router.use("/api/v1/import", importRouter());

app.use(cors());
app.use(bodyParser.json());
app.use(router);

app.listen(configuration.port, () => {
  console.log(`Api listening on port ${configuration.port}`);
});
