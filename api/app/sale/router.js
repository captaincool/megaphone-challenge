const Router = require("express");
const Sale = require("./model");
const { QueryTypes } = require("sequelize");
const database = require("../database");
const middlewareJsonRequest = require("../middleware/json/request");
const middlewareJsonResponse = require("../middleware/json/response");

/**
 * Get all sale entries and return them
 *
 * TODO: Add support for filtering and pagination
 */
const findAll = (req, res) => {
  return Sale.findAll().then(sales => {
    res.status(200).send({
      data: sales
    });
  });
};

/**
 * Calculate the total revenue for all sale entires and return the value
 *
 * TODO: Add support for filtering
 */
const calculateRevenue = async (req, res) => {
  const [result] = await database.query(
    "select sum(price * quantity) as revenue from sale",
    {
      type: QueryTypes.SELECT
    }
  );

  res.status(200).send({
    data: result
  });
};

/**
 * Get a single sale record and return it
 */
const findOneById = (req, res) => {
  return Sale.findOne({
    where: {
      id: req.params.id
    }
  }).then(sale => {
    res.status(200).send({
      data: sale
    });
  });
};

module.exports = () => {
  const router = Router({ mergeParams: true });

  // Apply json content headers
  router.use("/", middlewareJsonRequest, middlewareJsonResponse);

  router.get("/", findAll);
  router.get("/:id", findOneById);
  router.get("/aggregate/revenue", calculateRevenue);

  return router;
};
