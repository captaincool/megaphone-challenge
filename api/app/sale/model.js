const database = require("../database");
const Sequelize = require("sequelize");

module.exports = database.define(
  "sale",
  {
    id: {
      type: Sequelize.INTEGER,
      autoIncrement: true,
      primaryKey: true
    },
    quantity: {
      type: Sequelize.INTEGER,
      allowNull: false,
      unique: true,
      validate: {
        min: 0,
        isInt: true
      }
    },
    price: {
      type: Sequelize.NUMERIC(11, 2),
      allowNull: false,
      unique: true,
      validate: {
        min: 0,
        isNumeric: true
      }
    },
    product_description: {
      type: Sequelize.TEXT,
      unique: true,
      allowNull: false
    },
    customer_name: {
      type: Sequelize.TEXT,
      unique: true,
      allowNull: false
    },
    merchant_name: {
      type: Sequelize.TEXT,
      unique: true,
      allowNull: false
    },
    merchant_address: {
      type: Sequelize.TEXT,
      unique: true,
      allowNull: false
    },
    created_date: {
      type: Sequelize.DATE,
      allowNull: false
    },
    modified_date: {
      type: Sequelize.DATE
    },
    deleted_date: {
      type: Sequelize.DATE
    }
  },
  {
    createdAt: "created_date",
    updatedAt: "modified_date",
    deletedAt: "deleted_date"
  }
);
