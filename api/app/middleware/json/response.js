/**
 * Attach the JSON content type header to the response
 */
module.exports = (req, res, next) => {
  res.setHeader("content-type", "application/json");
  next();
};
