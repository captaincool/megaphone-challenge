/**
 * Ensure the incoming request contains JSON content type headers
 */
module.exports = (req, res, next) => {
  if (req.headers["content-type"] !== "application/json") {
    res.status(415).send({
      errors: [
        {
          status: 415,
          title: "Invalid content type"
        }
      ]
    });
    return;
  }

  next();
};
