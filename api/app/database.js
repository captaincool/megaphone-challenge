const Sequelize = require("sequelize");
const configuration = require("./configuration");

module.exports = new Sequelize(configuration.databaseUrl, {
  define: {
    freezeTableName: true
  }
});

Sequelize.postgres.DECIMAL.parse = value => Number(value);
