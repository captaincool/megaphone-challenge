const Router = require("express");
const importSaleRouter = require("./sale/router");

module.exports = () => {
  const router = Router({ mergeParams: true });
  router.use("/sale", importSaleRouter());
  return router;
};
