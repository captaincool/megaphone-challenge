const multer = require("multer");
const parse = require("csv-parse");
const storage = multer.memoryStorage();
const upload = multer({ storage: storage });
const Router = require("express");
const middlewareJsonResponse = require("../../middleware/json/response");

const SaleModel = require("../../sale/model");

/**
 * Import a batch of Sales based on a CSV file supplied in the request
 */
const importBulk = (req, res) => {
  if (!req.file) {
    res.status(400).send({
      errors: [
        {
          status: 400,
          title: "File is missing",
          detail: "The request must contain file contents"
        }
      ]
    });
    return;
  }

  // Parse the incoming file and process the contents
  // TODO: Add CSV file type validation
  parse(req.file["buffer"].toString(), async (err, output) => {
    if (err) {
      res.status(500).send({
        errors: [
          {
            status: 500,
            title: "Server error"
          }
        ]
      });
    }

    try {
      /**
       * Currently there is no way to uniquely reference orders, so to prevent duplicates
       * we create any record that doesn't exactly match one that already exists
       * TODO: Improve duplication checking to be smarter
       */
      const data = await Promise.all(
        output.slice(1).map(sale => {
          return new Promise(resolve => {
            SaleModel.findOrCreate({
              where: {
                customer_name: sale[0],
                product_description: sale[1],
                price: Number(sale[2]),
                quantity: Number(sale[3]),
                merchant_name: sale[4],
                merchant_address: sale[5]
              }
            }).then(
              ([record, created]) =>
                resolve({
                  status: created ? "created" : "ignored",
                  duplicate: !created,
                  data: record
                }),
              error =>
                resolve({
                  status: "failed",
                  data: sale,
                  message: error
                })
            );
          });
        })
      );

      res.status(201).send(
        data.reduce(
          (response, record) => {
            response[record.status].push(record.data);
            return response;
          },
          {
            created: [],
            ignored: [],
            failed: []
          }
        )
      );
    } catch (e) {
      res.status(500).send({
        errors: [
          {
            status: 500,
            title: "Server error"
          }
        ]
      });
    }
  });
};

module.exports = () => {
  const router = Router({ mergeParams: true });

  // Apply json response content headers
  router.use("/", middlewareJsonResponse);

  router.post("/", upload.single("file"), importBulk);
  return router;
};
