## Development

Please ensure you have the latest version of node installed. Run `npm install` while in this directory, then run `npm run start` to run the server on your local machine.

You should be able to connect to the api via [localhost:8080](localhost:8080)

## Testing

Run `npm run test` to run unit tests on the API
